## Name
Branch Details to CSV

## Description
Uses the rest api to pull all of the branches details and writes them to a CSV file


## Installation
Requires Python 3.7 or higher

## Usage
1. Edit the INI file to input the information about the Versa Director
2. Execute the file

## Support
No support implied or granted; this tool is downloaded as is.
