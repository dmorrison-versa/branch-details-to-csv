#!/usr/bin/env python3
"""
Very simple HTTP server in python for logging requests
Usage::
    ./server.py [<port>]
"""
from http.server import BaseHTTPRequestHandler, HTTPServer
import logging
import requests
import json
import traceback
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
import sqlite3
import pickle
import base64
import difflib
from datetime import datetime
import configparser
import smtplib
from email.mime.text import MIMEText
from sys import argv
import sys
import csv

class S(BaseHTTPRequestHandler):
    def _set_response(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def do_GET(self):
        logging.info("GET request,\nPath: %s\nHeaders:\n%s\n", str(self.path), str(self.headers))
        self._set_response()
        self.wfile.write("GET request for {}".format(self.path).encode('utf-8'))

    def do_POST(self):
        content_length = int(self.headers['Content-Length']) # <--- Gets the size of data
        post_data = self.rfile.read(content_length) # <--- Gets the data itself
        logging.info("POST request,\nPath: %s\nHeaders:\n%s\n\nBody:\n%s\n",
                str(self.path), str(self.headers), post_data.decode('utf-8'))

        self._set_response()
        self.wfile.write("POST request for {}".format(self.path).encode('utf-8'))
        resp = post_data.decode('utf-8')
        print("resp: {}".format(resp))
        print(type(resp))

        # get the device name from the http post message
        response_dict = json.loads(resp)
        # device_name = post_data.decode('utf-8')['deviceName']
        device_name = response_dict['deviceName']
        alarm_text = response_dict['alarmText']
        alarm_text_list = alarm_text.split(',')
        current_commit_time = alarm_text_list[2].split('(')[1].split(')')[0]

        # get the last configuration from the db
        previous_config, previous_commit_time = ConfigDB.get_latest_config(device_name)
        previous_config = previous_config.strip().splitlines()

        # get the configuration from the device
        current_config = DirectorRest.get_device_config(device_name)

        diff_lines = []
        diff_lines.append("Config change for device: {}".format(device_name))
        for line in difflib.unified_diff(previous_config,
                                         current_config.strip().splitlines(),
                                         fromfile='file1', tofile='file2', lineterm=''):
            diff_lines.append(line)
        diff_lines[1] = "--- {}".format(previous_commit_time)
        diff_lines[2] = "+++ {}".format(current_commit_time)
        # out = open("diff.txt", "w")
        # out.write("\n".join(diff_lines))
        self.do_email(device_name, "\n".join(diff_lines))
        ConfigDB.insert(device_name, current_config, current_commit_time)

    def do_email(self, device_name, email_text):
        global ini_path
        config = configparser.ConfigParser()
        config.read(ini_path)
        sender = config['email']['sender']
        receiver = config['email']['receiver']

        msg = MIMEText(email_text)

        msg['Subject'] = 'Config change for device: {}'.format(device_name)
        msg['From'] = sender
        msg['To'] = receiver

        user = config['email']['username']
        password = config['email']['password']

        smtp_host = config['email']['smtp_host']
        smtp_port = config['email']['smtp_port']

        with smtplib.SMTP(smtp_host, smtp_port) as server:
            use_tls = bool(config['email']['use_tls'])
            if use_tls:
                server.ehlo()
                server.starttls()
                server.ehlo()
            server.login(user, password)
            server.sendmail(sender, receiver, msg.as_string())
            print("mail successfully sent")


class ConfigDB:
    connection = None

    @staticmethod
    def initialize():
        global ini_path
        config = configparser.ConfigParser()
        config.read(ini_path)
        db_path = config['database']['db_path']
        ConfigDB.connection = sqlite3.connect(db_path)
        ConfigDB.create_tables()

    @staticmethod
    def deserialize(base64_string):
        """Return an object from a pickled string"""
        base64_bytes = base64_string.encode("utf-8")
        pickle_bytes = base64.b64decode(base64_bytes)
        obj = pickle.loads(pickle_bytes)
        return (obj)

    @staticmethod
    def serialize(obj):
        """Return a string representation of this object"""
        pickle_string = pickle.dumps(obj)
        base64_bytes = base64.b64encode(pickle_string)
        base64_string = base64_bytes.decode('utf-8')
        return (base64_string)

    @staticmethod
    def create_tables():
        """create the needed tables"""
        cursor = ConfigDB.connection.cursor()
        cursor.execute("CREATE TABLE IF NOT EXISTS configs (id INTEGER PRIMARY KEY AUTOINCREMENT, site TEXT, config TEXT, commit_time TEXT)")

    @staticmethod
    def insert(device_name, configuration, commit_time):
        """Insert the configuration into the db"""
        if not ConfigDB.connection:
            ConfigDB.initialize()

        configuration_serialized = ConfigDB.serialize(configuration)
        if not ConfigDB.is_config_in_db(configuration_serialized):
            command = ''' INSERT INTO configs (site, config, commit_time) VALUES (?, ?, ?) '''
            cursor = ConfigDB.connection.cursor()
            cursor.execute(command, (device_name, configuration_serialized, commit_time))
            ConfigDB.connection.commit()

    @staticmethod
    def is_config_in_db(configuration_serialized):
        if not ConfigDB.connection:
            ConfigDB.initialize()

        command = ''' SELECT id FROM configs WHERE (config = ?)'''
        cursor = ConfigDB.connection.cursor()
        cursor.execute(command, [configuration_serialized])
        rows = cursor.fetchall()
        if rows:
            return True
        return False

    @staticmethod
    def get_latest_config(device_name):
        """get the latest config for the device"""
        if not ConfigDB.connection:
            ConfigDB.initialize()
        command = "select config, commit_time from configs where id = (select max(id) from configs where site=?)"
        cursor = ConfigDB.connection.cursor()
        cursor.execute(command, [device_name])
        rows = cursor.fetchall()
        if rows:
            config = ConfigDB.deserialize(rows[0][0])
            commit_time = rows[0][1]
            return config, commit_time
        return "",""


class DirectorRest():
    director_ip = director_port = username = password = None

    @staticmethod
    def get_director_config_from_ini():
        if not DirectorRest.director_ip:
            global ini_path
            config = configparser.ConfigParser()
            config.read(ini_path)
            DirectorRest.director_ip = config['director']['director_ip']
            DirectorRest.director_port = config['director']['director_port']
            DirectorRest.username = config['director']['username']
            DirectorRest.password = config['director']['password']

    @staticmethod
    def get_orginization_list():
        DirectorRest.get_director_config_from_ini()
        url = 'https://{}:{}/vnms/organization/orgs?offset=0&limit=1000'
        print("getting organization list")
        response = requests.get(url.format(DirectorRest.director_ip, DirectorRest.director_port),
                                auth=(DirectorRest.username, DirectorRest.password), verify=False, timeout=(5, 120),
                                headers={'Accept': 'application/json', 'Content-Type': 'application/json'})
        response.raise_for_status()
        organizations_list = json.loads(response.text)['organizations']
        # print(organizations_list.keys())
        return organizations_list

    @staticmethod
    def get_device_details_per_org(org_name):
        DirectorRest.get_director_config_from_ini()
        url = "https://{}:{}/vnms/appliance/filter/{}?offset=0&limit=1000"
        response = requests.get(url.format(DirectorRest.director_ip, DirectorRest.director_port, org_name),
                                auth=(DirectorRest.username, DirectorRest.password), verify=False, timeout=(5, 120),
                                headers={'Accept': 'application/json', 'Content-Type': 'application/json'})
        response.raise_for_status()
        inventory = json.loads(response.text)
        return inventory['versanms.ApplianceStatusResult']['appliances']

    @staticmethod
    def get_appliance_list():
        DirectorRest.get_director_config_from_ini()
        url = 'https://{}:{}/vnms/cloud/systems/getAllAppliancesBasicDetails'
        print("getting device list")
        response = requests.get(url.format(DirectorRest.director_ip, DirectorRest.director_port),
                                auth=(DirectorRest.username, DirectorRest.password), verify=False, timeout=(5, 120),
                                headers={'Accept': 'application/json', 'Content-Type': 'application/json'})
        response.raise_for_status()
        appliance_list = response.json()['appliance-list']
        return appliance_list
        # try:
        #     # logging.info("response: {}".format(json.dumps(response, indent=4)))
        #     print(json.dumps(appliance_list, indent=4))
        # except:
        #     print(traceback.format_exc())

    @staticmethod
    def get_device_config(device_name):
        DirectorRest.get_director_config_from_ini()
        url = 'https://{}:{}/api/config/devices/device/{}/config?deep=true'
        response = requests.get(url.format(DirectorRest.director_ip, DirectorRest.director_port, device_name),
                                auth=(DirectorRest.username, DirectorRest.password), verify=False, timeout=(5, 120),
                                headers={'Accept': 'application/json', 'Content-Type': 'application/json'})
        response.raise_for_status()
        return response.text


def run(server_class=HTTPServer, handler_class=S, port=8080):
    logging.basicConfig(level=logging.INFO)
    server_address = ('', port)
    httpd = server_class(server_address, handler_class)
    logging.info('Starting httpd...\n')

    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass
    httpd.server_close()
    logging.info('Stopping httpd...\n')


if __name__ == '__main__':
    ini_path = 'director_rest.ini'
    csv_file_path = "branch_details.csv"
    if len(argv) == 2:
        ini_path = argv[1]

    config = configparser.ConfigParser()
    config.read(ini_path)

    appliances = {}
    org_list = DirectorRest.get_orginization_list()
    for org in org_list:
        org_name = org['name']
        org_appliances = DirectorRest.get_device_details_per_org(org_name)
        for appliance in org_appliances:
            appliances[appliance['name']] = appliance
    if org_appliances:
        f = open(csv_file_path, 'w')
        header = ['name',
                  'type',
                  'ipAddress',
                  'ping-status',
                  'sync-status',
                  'softwareVersion',
                  'SPack',
                  'OssPack']
        csv_file = csv.writer(f)
        csv_file.writerow(header)
        print("Creating {}".format(csv_file_path))

        for k,v in appliances.items():
            # print(json.dumps(v, indent=4))
            # print("name: {} spack: {} osspack: {}".format(v['name'], v.get('SPack', 'None'), v.get('OssPack', 'None')))
            row = []
            for column in header:
                row.append(v.get(column))
            csv_file.writerow(row)
        f.close()

